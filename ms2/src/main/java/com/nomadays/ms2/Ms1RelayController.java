package com.nomadays.ms2;

import java.util.Map;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
public class Ms1RelayController {

  private WebClient.Builder webClientBuilder;

  public Ms1RelayController(@LoadBalanced WebClient.Builder builder) {
    this.webClientBuilder = builder;
  }

  @GetMapping("/ms1")
  public Mono<Map> ms1() {
    return webClientBuilder.build().get().uri("http://ms1").retrieve().bodyToMono(Map.class);
  }
}
