package com.nomadays.ms1;

import java.util.HashMap;
import java.util.Map;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Ms1Application {


  public static void main(String[] args) {
    SpringApplication.run(Ms1Application.class, args);
  }

  @GetMapping
  public Map<String, Object> hello() {
    Map<String, Object> map = new HashMap<>();
    map.put("message", "hello");
    return  map;
  }

  @GetMapping("/hello")
  public Map<String, Object> hello2() {
    Map<String, Object> map = new HashMap<>();
    map.put("message", "Hello world");
    return  map;
  }
}
